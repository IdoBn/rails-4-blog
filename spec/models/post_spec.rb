require 'spec_helper'

describe Post do
	let(:comment1) { FactoryGirl.create(:comment) }
	let(:comment2) { FactoryGirl.create(:comment) }
	subject(:post) { FactoryGirl.build(:post) }

	context 'without a body' do
		subject(:post) { Post.new }

		it { should_not be_valid }
		it 'raises an error' do
			expect { post.save! }.to raise_error(
				ActiveRecord::RecordInvalid
			)
		end
	end

	context 'default scope' do
		(1..3).each do |i|
			FactoryGirl.create(:post)
		end

		it 'should be in DESC order' do
			Post.first.created_at.should > Post.unscoped.first.created_at
		end
	end

	it 'include comments' do
		post.comments << comment1
		post.comments << comment2
		subject.comments.should include(comment1)
		subject.comments.should include(comment2)  
	end

	it 'changes the number of posts' do
		expect { subject.save }.to change { Post.count }.by(1)
	end

	it {should be_an_instance_of(Post)}
end