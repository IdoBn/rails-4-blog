require 'spec_helper'

describe Comment do
	subject(:comment) { FactoryGirl.create(:comment) }

	it 'changes the number of comments' do
		expect { subject.save }.to change { Comment.count }.by(1)
	end

	context 'default scope' do
		(1..3).each do |i|
			FactoryGirl.create(:comment)
		end

		it 'should be in DESC order' do
			Comment.first.created_at.should > Comment.unscoped.first.created_at
		end
	end
	
	it 'is invalid without content' do
		comment = Comment.new
		comment.should_not be_valid
	end

end