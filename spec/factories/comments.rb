FactoryGirl.define do
	factory :comment do
		content { Faker::Lorem.sentences.join(' ') }
	end
end