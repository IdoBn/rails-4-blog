FactoryGirl.define do
	factory :post do |f|
		f.title { Faker::Lorem.sentence }
		f.body { Faker::Lorem.sentences.join(' ') }
		# comments factory: :comment
		comments {
			Array(1..3).sample.times.map do
        FactoryGirl.create(:comment)
      end
		}
	end
end