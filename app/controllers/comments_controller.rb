class CommentsController < ActionController::Base





  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :set_post, only: [:show, :edit, :update, :destroy, :new]

  # GET /posts/1/comments/1
  # GET /posts/1/comments/1.json
  def show
    #@comments = Comment.where(post_id: @post.id)
  end

  # GET /posts/1/comments/new
  def new
    @comment = Comment.new
  end

  # GET /posts/1/comments/edit
  def edit
  end

  # POST /posts/1/comments
  # POST /posts/1/comments.json
  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create(params[:comment].permit(:content))
    respond_to do |format|
      if @comment.save
        format.html { redirect_to post_path({id:@comment.post_id}), notice: 'Post was successfully created.' }
        format.json { render action: 'show', status: :created, location: @comment }
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1/comments/1
  # PATCH/PUT /posts/1/comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to post_comment_path({id: @comment.id, post_id:@comment.post_id}), notice: 'Post was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1/comments/1
  # DELETE /posts/1/comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to posts_url }
      format.json { head :no_content }
    end
  end





  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    def set_post
      if @comment
        @post = Post.find(@comment.post_id)
      elsif params[:id]
        @post = Post.find(params[:id])
      else
        @post = Post.find(params[:post_id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:content, :post_id)
    end
end
