class Comment < ActiveRecord::Base
	# relations
  belongs_to :post

  # validations
  validates :content, presence: true

  # scopes
  default_scope  { order(:created_at => :desc) }
end
