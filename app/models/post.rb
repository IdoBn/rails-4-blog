class Post < ActiveRecord::Base
	# relations
  has_many :comments, :dependent => :destroy

  # validations
  validates :body, presence: true

  # scopes
  default_scope  { order(:created_at => :desc) }
end
